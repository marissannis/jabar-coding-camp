// SOAL 1
var nilai = 66;

// JAWABAN SOAL 1
if (nilai >= 85) { console.log('indeksnya A') }
else if (nilai >= 75 && nilai < 85) { console.log("indeksnya B") }
else if (nilai >= 65 && nilai < 75) { console.log("indeksnya C") }
else if (nilai >= 55 && nilai < 65) { console.log("indeksnya D") }
else if (nilai < 55) { console.log("indeksnya E") }


// SOAL 2
var tanggal = 06;
var bulan = 12;
var tahun = 1994;

// JAWABAN SOAL 2
switch (bulan) {
    case 1: console.log(tanggal + " " + 'Januari' + " " + tahun);
        break
    case 2: console.log(tanggal + " " + "Februari" + " " + tahun);
        break
    case 3: console.log(tanggal + " " + "Maret" + " " + tahun);
        break
    case 4: console.log(tanggal + " " + 'April' + " " + tahun);
        break
    case 5: console.log(tanggal + " " + "Mei" + " " + tahun);
        break
    case 6: console.log(tanggal + " " + "Juni" + " " + tahun);
        break
    case 7: console.log(tanggal + " " + "Juli" + " " + tahun);
        break
    case 8: console.log(tanggal + " " + "Agustus" + " " + tahun);
        break
    case 9: console.log(tanggal + " " + "September" + " " + tahun);
        break
    case 10: console.log(tanggal + " " + "Oktober" + " " + tahun);
        break
    case 11: console.log(tanggal + " " + "November" + " " + tahun);
        break
    case 12: console.log(tanggal + " " + "Desember" + " " + tahun);
        break
    default: console.log(tanggal + " " + "Desember" + " " + tahun);
        break
}


// SOAL 3
// segitiga n=3
// segitiga n=7

// JAWABAN SOAL 3
function segitiga1(panjang) {
    let hasil = "";
    for (let i = 0; i < panjang; i++) {
        for (let j = 0; j <= i; j++) {
            hasil += '#';
        }
        hasil += '\n';
    }
    return hasil;
}
console.log(segitiga1(3));

function segitiga1(panjang) {
    let hasil = "";
    for (let i = 0; i < panjang; i++) {
        for (let j = 0; j <= i; j++) {
            hasil += '#';
        }
        hasil += '\n';
    }
    return hasil;
}
console.log(segitiga1(7));


// SOAL 4
// Output m=3
// Output m=5
// Output m=7
// Output m=10

// JAWABAN SOAL 4 
var tampilan = '';
for (var i = 1; i <= 10; i++) {
    if (i % 2 == 0) {
        for (var j = 1; j <= 10; j++) {
            if (j % 2 == 2) {
                tampilan += 'I love programming';
            } else tampilan += " "
        }
        tampilan += '\n';
    } else {
        for (var j = 1; j <= 10; j++) {
            if (j % 2 == 0) {
                tampilan += " ";
            } else tampilan += 'I love Javascript';
        }
        tampilan += '\n';
    } else {
        for (var j = 1; j <= 10; j++) {
            if (j % 2 == 0) {
                tampilan += " ";
            } else tampilan += 'I love VueJS';
        }
        tampilan += '\n';
    } else {
        for (var j = 1; j <= 10; j++) {
            if (j % 2 == 0) {
                tampilan += " ";
            } else tampilan += 'I love programming';
        }
        tampilan += '\n';
    } else {
        for (var j = 1; j <= 10; j++) {
            if (j % 2 == 0) {
                tampilan += " ";
            } else tampilan += 'I love Javascript';
        }
        tampilan += '\n';
    } else {
        for (var j = 1; j <= 10; j++) {
            if (j % 2 == 0) {
                tampilan += " ";
            } else tampilan += 'I love VueJS';
        }
        tampilan += '\n';
    } else {
        for (var j = 1; j <= 10; j++) {
            if (j % 2 == 0) {
                tampilan += " ";
            } else tampilan += 'I love Programming';
        }
        tampilan += '\n';

        console.log(tampilan);

        // ga gerti T_T