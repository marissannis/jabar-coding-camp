// SOAL 1
var pertama = 'saya sangat senang hari ini'
var kedua = 'belajar javascript itu keren'
// JAWABAN SOAL 1
var kata1 = pertama.substring(0, 4);
var kata2 = pertama.substring(12, 18);
var kata3 = kedua.substring(0, 7);
var kata4 = kedua.substring(8, 18).toUpperCase(8, 18);

console.log(kata1 + ' ' + kata2 + ' ' + kata3 + ' ' + kata4);


// SOAL 2
var kataPertama = '10';
var kataKedua = '2';
var kataKetiga = '4';
var kataKeempat = '6';
// JAWABAN SOAL 2
var angka1 = parseInt(kataPertama);
var angka2 = parseInt(kataKedua);
var angka3 = parseInt(kataKetiga);
var angka4 = parseInt(kataKeempat);

var jawaban = ((angka1 + angka2) * (-angka3 + angka4));

console.log(jawaban);


// SOAL 3
var kalimat = 'wah javascript itu keren sekali';
// JAWABAN 3
var a = kalimat.substring(0, 3);
var b = kalimat.substring(4, 14);
var c = kalimat.substring(15, 18);
var d = kalimat.substring(19, 24);
var e = kalimat.substring(25, 31);

console.log(a);
console.log(b);
console.log(c);
console.log(d);
console.log(e);