// soal 1
var tanggal = 14
var bulan = 3
var tahun = 2022

// jawaban soal 1

function next_date(tanggal, bulan, tahun) {
    return tanggal, bulan, tahun.split(' ');
}

switch (bulan) {
    case 1: console.log(tanggal + " " + 'Januari' + " " + tahun);
        break
    case 2: console.log(tanggal + " " + "Februari" + " " + tahun);
        break
    case 3: console.log(tanggal + " " + "Maret" + " " + tahun);
        break
    case 4: console.log(tanggal + " " + 'April' + " " + tahun);
        break
    case 5: console.log(tanggal + " " + "Mei" + " " + tahun);
        break
    case 6: console.log(tanggal + " " + "Juni" + " " + tahun);
        break
    case 7: console.log(tanggal + " " + "Juli" + " " + tahun);
        break
    case 8: console.log(tanggal + " " + "Agustus" + " " + tahun);
        break
    case 9: console.log(tanggal + " " + "September" + " " + tahun);
        break
    case 10: console.log(tanggal + " " + "Oktober" + " " + tahun);
        break
    case 11: console.log(tanggal + " " + "November" + " " + tahun);
        break
    case 12: console.log(tanggal + " " + "Desember" + " " + tahun);
        break
    default: console.log(tanggal + " " + "Desember" + " " + tahun);
        break
}


// soal 2
var kalimat_1 = 'Halo nama saya Marissa An Nisa'
var kalimat_2 = "Saya Rissa"
var kalimat_3 = "Saya Marissa An Nisa"

// jawaban soal 2
function jumlah_kata(kalimat_1) {
    return kalimat_1.split(" ").length;
}
console.log(jumlah_kata(kalimat_1))

function jumlah_kata(kalimat_2) {
    return kalimat_2.split(" ").length;
}
console.log(jumlah_kata(kalimat_2))

function jumlah_kata(kalimat_3) {
    return kalimat_3.split(" ").length;
}
console.log(jumlah_kata(kalimat_3))